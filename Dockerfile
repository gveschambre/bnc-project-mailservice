FROM node:10
ENV MAILJET_API_KEY=83afe6aff2839f820e51a87dce8431a3
ENV MAILJET_API_SECRET=a9465e68f805a6785feab4859be3966f

WORKDIR /app
ENV NODE_ENV production

COPY package.json .

RUN npm install

COPY . .

ENTRYPOINT ["./docker-entrypoint.sh"]

CMD ["start"]